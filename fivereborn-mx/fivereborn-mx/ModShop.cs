﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using NativeUI;
using CitizenFX.Core.UI;
using CitizenFX.Core.Native;
using System.Drawing;

namespace mxfivereborn
{
    class ModShop : BaseScript
    {
        enum ShopStatus
        {
            None = 0,
            Entering = 1,
            Inside = 2,
            Exiting = 3
        }

        private MenuPool _menuPool;
        private UIMenu repairMenu;
        private UIMenu mainMenu;
        private ShopStatus shopStatus = ShopStatus.None;

        public ModShop()
        {
            _menuPool = new MenuPool();

            //
            // Repair Menu
            repairMenu = new UIMenu("Mod Shop", "~b~Pimp your ride.");
            repairMenu.MouseControlsEnabled = false;

            _menuPool.Add(repairMenu);

            UIMenuItem repair = new UIMenuItem("Repair Vehicle", "Repairs vehicle you are currently in.");
            repairMenu.AddItem(repair);

            repairMenu.OnItemSelect += (sender, item, index) =>
            {
                LocalPlayer.Character.CurrentVehicle.Repair();
                repairMenu.Visible = false;
                mainMenu.Visible = true;
            };

            //
            // Main menu
            mainMenu = new UIMenu("Mod Shop", "~b~Pimp your ride.");
            mainMenu.MouseControlsEnabled = false;

            _menuPool.Add(mainMenu);

            mainMenu.OnItemSelect += (sender, item, index) =>
            {
                Screen.ShowNotification("Button pressed: " + item.Text);
            };

            _menuPool.RefreshIndex();

            Tick += OnTick;
        }


        private UIMenu newModShopItem(UIMenu parent, string text, string description, bool mouseControl = false)
        {
            UIMenu newMenu = _menuPool.AddSubMenu(parent, text, description);
            newMenu.MouseControlsEnabled = mouseControl;
            newMenu.OnMenuClose += (sender) => { sender.ParentMenu.Visible = true; };
            newMenu.OnItemSelect += ModShopEventHandler;
            return newMenu;
        }

        private UIMenu newModShopItem(string text, string description, bool mouseControl = false)
        {
            return newModShopItem(mainMenu, text, description, mouseControl);
        }

        private void ModShopEventHandler(UIMenu sender, UIMenuItem selectedItem, int index)
        {
            switch (sender.Title.Caption)
            {
                case "Armor":
                    break;
            }
        }

        private void LoadModShopMenu()
        {

        }

        void UnloadModshopMenus()
        {
            mainMenu.MenuItems.Clear();
        }

        private async Task OnTick()
        {
            _menuPool.ProcessMenus();

            if (shopStatus == ShopStatus.None && LocalPlayer.Character.IsInVehicle())
            {
                var veh = LocalPlayer.Character.CurrentVehicle;
                if (veh.Exists() && (veh.Model.IsCar || veh.Model.IsBike))
                {
                    if (World.GetDistance(LocalPlayer.Character.Position, new Vector3(726.9945f, -1088.869f, 21.92979f)) < 5f)
                    {
                        Function.Call(Hash._SET_VEHICLE_HALT, new InputArgument[] { LocalPlayer.Character.CurrentVehicle.Handle, 3.0f, 1, false });

                        Screen.Fading.FadeOut(1000);
                        shopStatus = ShopStatus.Entering;
                    }
                }
            }
            else if (shopStatus == ShopStatus.Entering)
            {
                if (Screen.Fading.IsFadedOut)
                {
                    var veh = LocalPlayer.Character.CurrentVehicle;

                    veh.Position = new Vector3(731.4163f, -1088.822f, 21.733f);
                    veh.Heading = 269.318f;
                    veh.PlaceOnGround();

                    veh.IsInvincible = true;
                    veh.LockStatus = VehicleLockStatus.Locked;
                    veh.IsPositionFrozen = true;
                    veh.IsCollisionEnabled = false;
                    //veh.Mods.InstallModKit();
                    Function.Call(Hash.SET_VEHICLE_MOD_KIT, veh.Handle, 0);

                    LocalPlayer.IsInvincible = true;

                    LoadModShopMenu();

                    shopStatus = ShopStatus.Inside;
                    Screen.Fading.FadeIn(400);

                    if (LocalPlayer.Character.CurrentVehicle.IsDamaged) repairMenu.Visible = true;
                    else mainMenu.Visible = true;
                }
            }
            else if (shopStatus == ShopStatus.Inside)
            {
                if (!_menuPool.IsAnyMenuOpen())
                {
                    Screen.Fading.FadeOut(600);
                    shopStatus = ShopStatus.Exiting;
                }
            }
            else if (shopStatus == ShopStatus.Exiting)
            {
                if (Screen.Fading.IsFadedOut)
                {
                    UnloadModshopMenus();

                    var veh = LocalPlayer.Character.CurrentVehicle;

                    veh.Position = new Vector3(710.9945f, -1088.869f, 21.92979f);
                    veh.Heading = 88.768f;
                    veh.PlaceOnGround();

                    veh.IsInvincible = false;
                    veh.LockStatus = VehicleLockStatus.None;
                    veh.IsPositionFrozen = false;
                    veh.IsCollisionEnabled = true;

                    LocalPlayer.IsInvincible = false;

                    shopStatus = ShopStatus.None;
                    Screen.Fading.FadeIn(400);
                }
            }

            await Task.FromResult(0);
        }
    }
}
